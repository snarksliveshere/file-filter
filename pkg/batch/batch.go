package batch

// GetBatch distributes the slice of elements to the workers
func GetBatch(ln, numWorkers int) [][]int {
	numFilesInBatch, _ := getBatchNums(ln, numWorkers)
	rangeSl := makeRange(0, ln)
	return LoopBatchHaveMax(rangeSl, ln, numFilesInBatch)
}

func getBatchNums(ln, max int) (int, int) {
	if ln < max {
		return 1, 0
	}
	res := ln / max
	remainder := ln % max
	if remainder > 0 {
		return res, remainder
	}
	return res, 0
}

// LoopBatchHaveMax splits by the number of elements in batch.
// gives a slice of data with indexes
func LoopBatchHaveMax(sl []int, max, batch int) [][]int {
	if len(sl) == 0 || max <= 0 || batch <= 0 {
		return nil
	}
	out := make([][]int, 0)
	for i := 0; i < len(sl); i += batch {
		j := i + batch
		if j > len(sl) {
			j = len(sl)
		}
		if j > max {
			j = max
		}
		tmp := append([]int(nil), i, j)
		out = append(out, tmp)
		if j == max {
			break
		}
	}
	return out
}

func makeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}
