package batch

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type (
	loopBatchIn struct {
		max, batch int
		sl         []int
	}
	loopBatchOut struct {
		ln                 int
		lastStart, lastEnd int
	}
)

func TestLoopBatchHaveMax(t *testing.T) {
	t.Run("TestLoopBatchHaveMax success", func(t *testing.T) {
		casesSuccess := []struct {
			in  loopBatchIn
			out loopBatchOut
		}{
			{
				in: loopBatchIn{
					max:   20,
					batch: 5,
					sl:    make([]int, 60),
				},
				out: loopBatchOut{
					ln:        4,
					lastStart: 15, lastEnd: 20,
				},
			},
			{
				in: loopBatchIn{
					max:   100,
					batch: 50,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln:        1,
					lastStart: 0, lastEnd: 10,
				},
			},
			{
				in: loopBatchIn{
					max:   500,
					batch: 150,
					sl:    make([]int, 100),
				},
				out: loopBatchOut{
					ln:        1,
					lastStart: 0, lastEnd: 100,
				},
			},
			{
				in: loopBatchIn{
					max:   20,
					batch: 20,
					sl:    make([]int, 100),
				},
				out: loopBatchOut{
					ln:        1,
					lastStart: 0, lastEnd: 20,
				},
			},
			{
				in: loopBatchIn{
					max:   20,
					batch: 20,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln:        1,
					lastStart: 0, lastEnd: 10,
				},
			},
			{
				in: loopBatchIn{
					max:   200,
					batch: 20,
					sl:    make([]int, 100),
				},
				out: loopBatchOut{
					ln:        5,
					lastStart: 80, lastEnd: 100,
				},
			},
			{
				in: loopBatchIn{
					max:   200,
					batch: 20,
					sl:    make([]int, 5),
				},
				out: loopBatchOut{
					ln:        1,
					lastStart: 0, lastEnd: 5,
				},
			},
		}
		for _, v := range casesSuccess {
			t.Run("TestLoopBatchHaveMax", func(t *testing.T) {
				res := LoopBatchHaveMax(v.in.sl, v.in.max, v.in.batch)
				assert.Equal(t, v.out.ln, len(res))
				assert.Equal(t, v.out.lastStart, res[len(res)-1][0])
				assert.Equal(t, v.out.lastEnd, res[len(res)-1][1])
			})
		}
	})

	t.Run("TestLoopBatchHaveMax Check NIL", func(t *testing.T) {
		casesNil := []struct {
			in  loopBatchIn
			out loopBatchOut
		}{
			{
				in: loopBatchIn{
					max:   20,
					batch: 5,
					sl:    make([]int, 0),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
			{
				in: loopBatchIn{
					max:   50,
					batch: 0,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
			{
				in: loopBatchIn{
					max:   0,
					batch: 10,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
			{
				in: loopBatchIn{
					max:   0,
					batch: 0,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
			{
				in: loopBatchIn{
					max:   -10,
					batch: 0,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
			{
				in: loopBatchIn{
					max:   45,
					batch: -200,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln: 0,
				},
			},
		}
		for _, v := range casesNil {
			t.Run("TestLoopBatchHaveMax Empty case", func(t *testing.T) {
				res := LoopBatchHaveMax(v.in.sl, v.in.max, v.in.batch)
				assert.Equal(t, v.out.ln, len(res))
				assert.Nil(t, res)
			})
		}
	})

	t.Run("TestLoopBatchHaveMax error", func(t *testing.T) {
		casesError := []struct {
			in  loopBatchIn
			out loopBatchOut
		}{
			{
				in: loopBatchIn{
					max:   20,
					batch: 5,
					sl:    make([]int, 60),
				},
				out: loopBatchOut{
					ln:        12,
					lastStart: 55, lastEnd: 60,
				},
			},
			{
				in: loopBatchIn{
					max:   100,
					batch: 50,
					sl:    make([]int, 10),
				},
				out: loopBatchOut{
					ln:        0,
					lastStart: 50, lastEnd: 100,
				},
			},
			{
				in: loopBatchIn{
					max:   500,
					batch: 150,
					sl:    make([]int, 100),
				},
				out: loopBatchOut{
					ln:        2,
					lastStart: 100, lastEnd: 150,
				},
			},
		}
		for _, v := range casesError {
			t.Run("TestLoopBatchHaveMax", func(t *testing.T) {
				res := LoopBatchHaveMax(v.in.sl, v.in.max, v.in.batch)
				assert.NotEqual(t, v.out.ln, len(res))
				assert.NotEqual(t, v.out.lastStart, res[len(res)-1][0])
				assert.NotEqual(t, v.out.lastEnd, res[len(res)-1][1])
			})
		}
	})
}
