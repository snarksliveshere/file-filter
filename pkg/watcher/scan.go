package watcher

import (
	"context"
	"os"
	"sync"
	"time"

	"gitlab.com/snarksliveshere/file-filter/pkg/batch"
	"gitlab.com/snarksliveshere/file-filter/pkg/logger"
)

const (
	OpCreate = "CREATE"
	OpModify = "MODIFY"
	OpDelete = "DELETE"
)

// Event struct event chan
type Event struct {
	Name     string
	FileName string
	FileInfo os.FileInfo
	Err      error
}

// FileWatcher struct for scan folder
type FileWatcher struct {
	log logger.Logger

	hotPath    string
	numWorkers int
	sleepTime  time.Duration

	mu        sync.RWMutex
	destFiles map[string]os.FileInfo

	Evt chan Event
}

// NewFileWatcher  init new file watcher
func NewFileWatcher(logger logger.Logger, destPath string, numWorkers int) FileWatcher {
	return FileWatcher{
		log:        logger,
		hotPath:    destPath,
		numWorkers: numWorkers,
		Evt:        make(chan Event),
		sleepTime:  time.Millisecond,
	}
}

// FileWatch interface provides access to file watcher
type FileWatch interface {
	AddDirFiles() error
	ScanDir(ctx context.Context)
}

// ScanDir scan destination folder
func (f *FileWatcher) ScanDir(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		filesUpdate, err := GetDirFiles(f.log, f.hotPath, f.numWorkers)
		if err != nil {
			f.Evt <- Event{Err: err}
			continue
		}
		if len(filesUpdate) == 0 {
			continue
		}

		batchFiles := batch.GetBatch(len(filesUpdate), f.numWorkers)

		done := make(chan struct{})

		for _, v := range batchFiles {
			go func(dir []os.FileInfo) {
				f.readEvents(dir)
				done <- struct{}{}
			}(filesUpdate[v[0]:v[1]])
		}

		for range batchFiles {
			<-done
		}

		f.mu.Lock()
		m := make(map[string]os.FileInfo, len(filesUpdate))
		for _, v := range filesUpdate {
			m[v.Name()] = v
		}
		f.destFiles = m
		f.mu.Unlock()
		time.Sleep(f.sleepTime)
	}
}

// AddDirFiles receives files asynchronously
func (f *FileWatcher) AddDirFiles() error {
	var err error
	f.destFiles, err = GetDirFilesMap(f.log, f.hotPath, f.numWorkers)
	if err != nil {
		return err
	}

	return nil
}

func (f *FileWatcher) readEvents(info []os.FileInfo) {
	for _, value := range info {
		f.readEvent(value.Name(), value)
	}
}

func (f *FileWatcher) readEvent(name string, info os.FileInfo) {
	f.mu.RLock()
	oldFile, ok := f.destFiles[name]
	f.mu.RUnlock()
	if !ok {
		f.Evt <- Event{Name: OpCreate, FileName: name, FileInfo: info}
		return
	}
	if oldFile.Size() != info.Size() {
		f.Evt <- Event{Name: OpModify, FileName: name, FileInfo: info}
	}
}
