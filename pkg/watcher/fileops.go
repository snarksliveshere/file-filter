package watcher

import (
	"errors"
	"fmt"
	"os"
	"sync"

	"gitlab.com/snarksliveshere/file-filter/pkg/batch"
	"gitlab.com/snarksliveshere/file-filter/pkg/logger"
)

func getOSDir(path string) ([]os.DirEntry, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if !stat.IsDir() {
		return nil, err
	}
	files, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}
	return files, nil
}

// GetDirFiles get files from folder async with workers
func GetDirFiles(log logger.Logger, path string, numWorkers int) ([]os.FileInfo, error) {
	//TODO: dependency from Logger isn't so good, can be fixed in the future
	files, err := getOSDir(path)
	if err != nil {
		return nil, err
	}
	ln := len(files)
	if ln == 0 {
		return nil, nil
	}

	ch := make(chan os.FileInfo)
	endFillSet := make(chan struct{})
	endRangeFiles := make(chan struct{})
	batchFiles := batch.GetBatch(ln, numWorkers)

	mu := sync.Mutex{}

	sl := make([]os.FileInfo, 0, len(files))

	go func() {
		for {
			select {
			case val := <-ch:
				mu.Lock()
				sl = append(sl, val)
				mu.Unlock()
			case <-endRangeFiles:
				endFillSet <- struct{}{}
				return
			default:
			}
		}
	}()

	done := make(chan struct{})

	for _, v := range batchFiles {
		go func(dir []os.DirEntry) {
			rangeFiles(log, dir, ch)
			done <- struct{}{}
		}(files[v[0]:v[1]])
	}

	for range batchFiles {
		<-done
	}
	endRangeFiles <- struct{}{}

	<-endFillSet

	res := make([]os.FileInfo, 0, len(sl))

	mu.Lock()
	res = append([]os.FileInfo(nil), sl...)
	mu.Unlock()

	return res, nil
}

// GetDirFilesMap convert slice files to map
func GetDirFilesMap(log logger.Logger, path string, numWorkers int) (map[string]os.FileInfo, error) {
	sl, err := GetDirFiles(log, path, numWorkers)
	if err != nil {
		return nil, err
	}
	m := make(map[string]os.FileInfo, len(sl))
	for _, v := range sl {
		m[v.Name()] = v
	}
	return m, nil
}

func rangeFiles(log logger.Logger, files []os.DirEntry, ch chan os.FileInfo) {
	for _, v := range files {
		info, err := v.Info()
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				continue
			}
			log.Error("can't get file info", err)
			continue
		}
		if !info.Mode().IsRegular() {
			log.Error(fmt.Sprintf("this isn't a regular file:[[%s]]", info.Name()), nil)

			continue
		}
		ch <- info
	}
}
