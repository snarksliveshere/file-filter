package logger

import (
	"fmt"
	"log"
	"os"
)

const (
	logInfoPrefix  = "[INFO] "
	logErrorPrefix = "[ERROR] "
)

// Logger logger struct
type Logger struct {
	*log.Logger
}

// NewLogger creates new logger
func NewLogger(path string) (Logger, error) {
	f, err := os.OpenFile(path,
		os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return Logger{}, err
	}
	l := log.New(f, "", log.LstdFlags)
	l.SetPrefix(logInfoPrefix)

	return Logger{
		Logger: l,
	}, nil
}

// Info logger info method
func (l *Logger) Info(msg string) {
	l.Logger.SetPrefix(logInfoPrefix)
	l.Logger.Println(msg)
}

// InfoWithStdOut  writes info to file and stdOut
func (l *Logger) InfoWithStdOut(msg string) {
	l.Logger.SetPrefix(logInfoPrefix)
	l.Logger.Println(msg)
	log.Println(msg)
}

// Error writes error to file and stdOut
func (l *Logger) Error(msg string, err error) {
	l.Logger.SetPrefix(logErrorPrefix)
	str := fmt.Sprintf("%s error:%v", msg, err)
	l.Logger.Println(str)
	log.Println(str)
	l.Logger.SetPrefix(logInfoPrefix)
}
