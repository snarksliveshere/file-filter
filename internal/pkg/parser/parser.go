package parser

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	envDestinationFolderPath = "DESTINATION_FOLDER_PATH"
	envBackupFolderPath      = "BACKUP_FOLDER_PATH"
)

// ParseCLIPath parses cli params
func ParseCLIPath() (hotPath, backupPath, logPath, statePath string) {
	flag.StringVar(&hotPath, "hot", "", "hot destination folder path")
	flag.StringVar(&backupPath, "backup", "", "backup folder path")
	flag.StringVar(&logPath, "log", "text.log", "log file")
	flag.StringVar(&statePath, "state", "state.json", "state file")

	flag.Parse()

	if hotPath == "" {
		hotPath = getEnvValue(envDestinationFolderPath, os.Environ())
	}
	if backupPath == "" {
		backupPath = getEnvValue(envBackupFolderPath, os.Environ())
	}

	if hotPath == "" || backupPath == "" {
		str := fmt.Sprintf("hotPath:[[%s]] && backupPath:[[%s]] must exist", hotPath, backupPath)
		log.Fatal(str)
	}

	return
}

func getEnvValue(value string, envs []string) string {
	prefixEnv := value + "="
	for _, v := range envs {
		if strings.HasPrefix(v, prefixEnv) {
			return strings.TrimPrefix(v, prefixEnv)
		}
	}
	return ""
}
