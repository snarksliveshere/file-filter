package state

import (
	"encoding/json"
	"log"
	"os"
)

// AppState state json structure
type AppState struct {
	HotPath    string `yaml:"hotPath"`
	BackupPath string `yaml:"backupPath"`
	LogPath    string `yaml:"logPath"`
}

// State I'm not quite sure what that means in this case
func State(hotPath, backupPatch, logPath, statePath string) error {
	b, err := os.ReadFile(statePath)
	if err != nil {
		if os.IsNotExist(err) {
			return stateSet([]AppState{{
				HotPath:    hotPath,
				BackupPath: backupPatch,
				LogPath:    logPath,
			}}, statePath)
		}
		return err
	}

	var prev []AppState
	err = json.Unmarshal(b, &prev)
	if err != nil {
		return err
	}
	actual := AppState{
		HotPath:    hotPath,
		BackupPath: backupPatch,
		LogPath:    logPath,
	}

	isEqual := isDataEqual(prev, actual)
	if !isEqual {
		return stateSet(append(prev, actual), statePath)
	}
	return nil
}

func isDataEqual(prev []AppState, actual AppState) bool {
	if len(prev) == 0 {
		return false
	}

	last := prev[len(prev)-1]
	if last.HotPath != actual.HotPath || last.BackupPath != actual.BackupPath ||
		last.LogPath != actual.LogPath {
		return false
	}
	return true
}

func stateSet(s []AppState, statePath string) error {
	file, err := os.OpenFile(statePath,
		os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	b, err := json.Marshal(s)
	if err != nil {
		return err
	}
	_, err = file.Write(b)
	return err
}
