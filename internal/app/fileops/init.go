package fileops

import (
	"fmt"
	"os"
	"sync"

	"gitlab.com/snarksliveshere/file-filter/pkg/batch"
	"gitlab.com/snarksliveshere/file-filter/pkg/watcher"
)

// CheckFilesOnStart Checks the files when the application starts
func (f *fileHandler) CheckFilesOnStart() error {
	destFiles, err := watcher.GetDirFiles(f.log, string(f.hotPath), f.numWorkers)
	if err != nil {
		return err
	}

	if len(destFiles) == 0 {
		return nil
	}
	mu := &sync.RWMutex{}
	backupFiles, err := watcher.GetDirFilesMap(f.log, string(f.backupPath), f.numWorkers)
	if err != nil {
		return err
	}

	batchFiles := batch.GetBatch(len(destFiles), f.numWorkers)

	done := make(chan struct{})

	for _, v := range batchFiles {
		go func(dir []os.FileInfo) {
			f.checkFiles(dir, backupFiles, mu)
			done <- struct{}{}
		}(destFiles[v[0]:v[1]])
	}

	for range batchFiles {
		<-done
	}

	return nil
}

func (f *fileHandler) checkFiles(destFiles []os.FileInfo, backupFiles map[string]os.FileInfo, mu *sync.RWMutex) {
	for _, v := range destFiles {
		tmp := fmt.Sprintf("%s.bak", v.Name())
		mu.RLock()
		backed, ok := backupFiles[tmp]
		mu.RUnlock()
		if ok && backed.Size() != v.Size() {
			f.logInfoFiles(v.Name(), watcher.OpModify, "on start")
			err := f.createOrCopyBackupFile(f.backupPath, f.hotPath, v.Name())
			if err != nil {
				f.log.Error(fmt.Sprintf("can't %s file:[[%s]]", watcher.OpModify, v.Name()), err)
			}
			continue
		}
		if !ok && f.checkIfDelete(v.Name()) {
			f.logInfoFile(v.Name(), watcher.OpDelete)
			f.logInfoFile(f.getDeleteFileName(v.Name()), watcher.OpDelete)

			err := f.handleDeleteFile(watcher.Event{FileName: v.Name()})
			if err != nil {
				f.log.Error(fmt.Sprintf("can't delete on start file[[%s]]", v.Name()), err)
			}
			continue
		}
		if !ok {
			f.logInfoFiles(v.Name(), watcher.OpCreate, "on start")
			err := f.createOrCopyBackupFile(f.backupPath, f.hotPath, v.Name())
			if err != nil {
				f.log.Error(fmt.Sprintf("can't create on start file[[%s]]", v.Name()), err)
			}
		}
	}
}
