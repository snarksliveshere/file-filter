package fileops

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/snarksliveshere/file-filter/pkg/watcher"
)

func (f *fileHandler) watchDeferredDeletedFiles(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		t := time.Now().UTC()
		sl := make([]string, 0)

		f.muDef.RLock()
		for name, value := range f.deferredDelete {
			if t.After(value.timeToDelete) {
				f.logInfoFile(value.backupName, watcher.OpDelete)
				f.logInfoFile(value.hotName, watcher.OpDelete)
				err := f.removeBunchFiles(value.backupName, value.hotName)
				if err != nil {
					f.log.Error(fmt.Sprintf("can't remove deferred delete backup file:[[%s]]", value.backupName), err)
					continue
				}
				sl = append(sl, name)
			}
		}
		f.muDef.RUnlock()

		f.muDef.Lock()
		for _, v := range sl {
			delete(f.deferredDelete, v)
		}
		f.muDef.Unlock()
		time.Sleep(time.Second)
	}
}

func (f *fileHandler) removeBunchFiles(paths ...string) error {
	for _, v := range paths {
		err := os.Remove(v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (f *fileHandler) checkIfDelete(name string) bool {
	return strings.HasPrefix(name, fmt.Sprintf("%s_", deletePrefix))
}

func (f *fileHandler) isDeleteFileHasISO8601(name string) (bool, time.Time, string) {
	str := strings.TrimPrefix(name, fmt.Sprintf("%s_", deletePrefix))
	index := strings.Index(str, "_")
	if index <= 0 {
		return false, time.Time{}, ""
	}
	possibleIso, err := time.Parse(time.RFC3339, str[:index])
	if err != nil {
		return false, time.Time{}, ""
	}
	filename := strings.TrimPrefix(str, fmt.Sprintf("%s_", str[:index]))

	return true, possibleIso, filename
}

func (f *fileHandler) getDeleteFileName(name string) string {
	str := strings.TrimPrefix(name, fmt.Sprintf("%s_", deletePrefix))
	index := strings.Index(str, "_")
	if index <= 0 {
		return str
	}
	_, err := time.Parse(time.RFC3339, str[:index])
	if err != nil {
		return str
	}
	return strings.TrimPrefix(str, fmt.Sprintf("%s_", str[:index]))
}

func (f *fileHandler) handleDeferredDeleteFile(fullName, fileName string, timeToDelete time.Time) error {
	hotpath := fmt.Sprintf("%s/%s", f.hotPath, fullName)
	backupPath := createBackupPathFromName(f.backupPath, fileName)
	_, err := os.Stat(backupPath)
	if err != nil {
		if os.IsNotExist(err) {
			err = CopyFile(hotpath, backupPath)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}

	f.muDef.RLock()
	_, ok := f.deferredDelete[fileName]
	f.muDef.RUnlock()
	if ok {
		return nil
	}

	el := fileToDelete{
		hotName:      hotpath,
		backupName:   backupPath,
		timeToDelete: timeToDelete.UTC(),
	}
	f.muDef.Lock()
	f.deferredDelete[fileName] = el
	f.muDef.Unlock()

	return nil
}

func (f *fileHandler) handleDeleteFile(evt watcher.Event) error {
	isIso, timeToDelete, filename := f.isDeleteFileHasISO8601(evt.FileName)
	if isIso {
		return f.handleDeferredDeleteFile(evt.FileName, filename, timeToDelete)
	}

	hotpath := fmt.Sprintf("%s/%s", f.hotPath, evt.FileName)

	_, err := os.Stat(createBackupPathFromName(f.backupPath, trimDelPrefix(evt.FileName)))
	if err != nil {
		if os.IsNotExist(err) {
			return f.removeBunchFiles(hotpath)
		}
		return err
	}

	backupPath := fmt.Sprintf("%s/%s.bak", f.backupPath, trimDelPrefix(evt.FileName))
	return f.removeBunchFiles(hotpath, backupPath)
}
