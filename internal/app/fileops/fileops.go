package fileops

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func createBackupPathFromName(backupPath BackupPathType, name string) string {
	return fmt.Sprintf("%s/%s.bak", backupPath, name)
}

func fileHasDeletePrefix(name string) bool {
	return strings.HasPrefix(name, fmt.Sprintf("%s_", deletePrefix))
}

func trimDelPrefix(name string) string {
	return strings.TrimPrefix(name, fmt.Sprintf("%s_", deletePrefix))
}

func (f *fileHandler) createOrCopyBackupFile(backupPath BackupPathType, hotPath HotPathType, fileName string) error {
	hotFilePath := fmt.Sprintf("%s/%s", hotPath, fileName)
	backupFilePath := createBackupPathFromName(backupPath, fileName)

	return CopyFile(hotFilePath, backupFilePath)
}

// CopyFile copy from - to files
func CopyFile(from, to string) error {
	source, err := os.Open(from)
	if err != nil {
		return err
	}
	defer func() {
		if err := source.Close(); err != nil {
			log.Print(err)
		}
	}()

	destination, err := os.Create(to)
	if err != nil {
		return err
	}
	//TODO: I'm not sure it's that necessary.
	err = destination.Sync()
	if err != nil {
		return err
	}
	defer func() {
		if err := destination.Close(); err != nil {
			log.Print(err)
		}
	}()
	_, err = io.Copy(destination, source)
	return err
}
