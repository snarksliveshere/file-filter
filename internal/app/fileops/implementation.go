package fileops

import (
	"context"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"gitlab.com/snarksliveshere/file-filter/pkg/logger"
	"gitlab.com/snarksliveshere/file-filter/pkg/watcher"
)

// HotPathType create type for hotPath
type HotPathType string

// BackupPathType create type for backupPath
type BackupPathType string

const (
	deletePrefix = "delete"
)

type fileToDelete struct {
	hotName      string
	backupName   string
	timeToDelete time.Time
}

type fileHandler struct {
	log        logger.Logger
	hotPath    HotPathType
	backupPath BackupPathType
	numWorkers int

	watcher watcher.FileWatcher

	timeCheck time.Time
	sleepTime time.Duration

	muDef          sync.RWMutex
	deferredDelete map[string]fileToDelete
}

// NewFileHandler create new fileHandler
func NewFileHandler(
	logger logger.Logger,
	hotPath HotPathType,
	backupPath BackupPathType,
	numWorkers int,
) (*fileHandler, error) {
	return &fileHandler{
		log:            logger,
		hotPath:        hotPath,
		backupPath:     backupPath,
		numWorkers:     numWorkers,
		timeCheck:      time.Now().UTC(),
		sleepTime:      time.Nanosecond,
		watcher:        watcher.NewFileWatcher(logger, string(hotPath), numWorkers),
		deferredDelete: make(map[string]fileToDelete),
	}, nil
}

// FileHandler provides fileHandler functional
type FileHandler interface {
	Run()
}

// Run starts the scanner and reacts to events
func (f *fileHandler) Run(ctx context.Context) {
	err := f.watcher.AddDirFiles()
	if err != nil {
		log.Fatal(err)
	}

	go f.watchDeferredDeletedFiles(ctx)
	go f.watcher.ScanDir(ctx)

	for {
		select {
		case <-ctx.Done():
			return
		case evt := <-f.watcher.Evt:
			if evt.Name == watcher.OpModify || !fileHasDeletePrefix(evt.FileName) {
				f.logInfoFiles(evt.FileName, evt.Name, "")
				err = f.createOrCopyBackupFile(f.backupPath, f.hotPath, evt.FileName)
				if err != nil {
					f.log.Error(fmt.Sprintf("can't %s backup file:[[%s]]", evt.Name, evt.FileName), err)
				}
				continue
			}
			f.logInfoFiles(evt.FileName, watcher.OpDelete, "")
			err = f.handleDeleteFile(evt)
			if err != nil {
				f.log.Error(fmt.Sprintf("can't handle delete file:[[%s]]", evt.FileInfo.Name()), err)
				continue
			}
		default:
		}
		time.Sleep(f.sleepTime)
	}
}

func (f *fileHandler) logInfoFiles(fileName, op, add string) {
	f.log.Printf("%s file:[[%s]] event:[[%s]] in folder:[[%s]]", add, fileName, strings.ToLower(op), f.hotPath)
	f.log.Printf("%s backup file:[[%s.bak]] event:[[%s]] in folder:[[%s]]", add, fileName, strings.ToLower(op), f.backupPath)
}

func (f *fileHandler) logInfoFile(fileName, op string) {
	f.log.Printf("file:[[%s] event:[[%s]", fileName, strings.ToLower(op))
}
