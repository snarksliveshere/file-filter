package scanlog

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"

	"gitlab.com/snarksliveshere/file-filter/pkg/logger"
)

type logscan struct {
	log  logger.Logger
	path string
}

// NewLogScan creates new log scanner
func NewLogScan(log logger.Logger, path string) *logscan {
	return &logscan{
		log:  log,
		path: path,
	}
}

// LogFilter provides log scan functionality
type LogFilter interface {
	ScanStdIn(ctx context.Context)
}

func (l *logscan) ScanStdIn(ctx context.Context) {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		fmt.Print("-> ")
		scanner.Scan()
		err := scanner.Err()
		if err != nil {
			l.log.Error("get err from scanner", err)
		}
		for _, v := range l.readLog(scanner.Text()) {
			fmt.Println(v)
		}
	}
}

func (l *logscan) readLog(find string) []string {
	regx, err := regexp.Compile(find)
	if err != nil {
		l.log.Error("can't compile regex", err)
		return nil
	}
	f, err := os.Open(l.path)
	if err != nil {
		l.log.Error(fmt.Sprintf("can't open file:[[%s]]", l.path), err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			l.log.Error("can't close file", err)
		}
	}()
	res := make([]string, 0, 10)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		lg := sc.Text()
		fileName := getFileNameFromLog(lg)
		if fileName != "" && regx.MatchString(fileName) {
			res = append(res, sc.Text())
			continue
		}
		if strings.Contains(strings.ToLower(lg), strings.ToLower(find)) {
			res = append(res, sc.Text())
		}
	}
	return res
}

func getFileNameFromLog(lg string) string {
	firstPos := "file:[["
	secondPos := "]] "

	index := strings.Index(lg, firstPos)
	if index < 0 {
		return ""
	}
	substr := strings.TrimPrefix(lg[index:], firstPos)
	index = strings.Index(substr, secondPos)
	if index < 0 {
		return ""
	}
	fileName := substr[:index]
	return fileName
}
