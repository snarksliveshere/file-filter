package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/snarksliveshere/file-filter/internal/app/fileops"
	"gitlab.com/snarksliveshere/file-filter/internal/app/scanlog"
	"gitlab.com/snarksliveshere/file-filter/internal/pkg/parser"
	"gitlab.com/snarksliveshere/file-filter/internal/pkg/state"
	"gitlab.com/snarksliveshere/file-filter/pkg/logger"
)

const (
	numWorkers = 10
)

// cfg & args
func main() {
	hot, backup, logPath, statePath := parser.ParseCLIPath()

	lg, err := logger.NewLogger(logPath)
	fatalIfErr(lg, err, "can't init lg")

	lg.Info("application is starting")

	err = state.State(hot, backup, logPath, statePath)
	if err != nil {
		fatalIfErr(lg, err, "can't init state")
	}

	fileHandler, err := fileops.NewFileHandler(lg, fileops.HotPathType(hot), fileops.BackupPathType(backup), numWorkers)
	fatalIfErr(lg, err, "cant run app")

	err = fileHandler.CheckFilesOnStart()
	fatalIfErr(lg, err, "can't check file on start")
	ctx, cancel := context.WithCancel(context.Background())

	go fileHandler.Run(ctx)

	logscan := scanlog.NewLogScan(lg, logPath)

	go logscan.ScanStdIn(ctx)
	terminate(lg, cancel)
}

func terminate(log logger.Logger, cancel context.CancelFunc) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)

	oscall := <-c
	log.InfoWithStdOut(fmt.Sprintf("finished with signal: %+v...", oscall))
	cancel()

	baseContext := context.Background()
	ctxShutDown, cancel := context.WithTimeout(baseContext, 2*time.Second)
	defer func() {
		cancel()
	}()

	<-ctxShutDown.Done()

	log.InfoWithStdOut("finished application")
}

func fatalIfErr(lg logger.Logger, err error, what string) {
	if err != nil {
		lg.Error(what, err)
		os.Exit(1)
	}
}
