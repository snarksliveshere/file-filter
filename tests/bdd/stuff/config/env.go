//go:build bdd_tests
// +build bdd_tests

package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

const envPrefix = ""

type Env struct {
	TestHotPath    string `envconfig:"TEST_HOT_PATH" default:"/home/snarksliveshere/go/src/file-filter/folders/destination"`
	TestBackupPath string `envconfig:"TEST_BACKUP_PATH" default:"/home/snarksliveshere/go/src/file-filter/folders/backup"`
	TestPath       string `envconfig:"TEST_PATH" default:"../features"`
}

func GetTestEnv() (Env, error) {
	var cfg Env
	err := envconfig.Process(envPrefix, &cfg)
	return cfg, err
}

func GetConfigs() Env {
	var cfg Env
	err := envconfig.Process(envPrefix, &cfg)
	if err != nil {
		log.Fatalf("cant get config, err: [%s]", err.Error())
	}
	return cfg
}
