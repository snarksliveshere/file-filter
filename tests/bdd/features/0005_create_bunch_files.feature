Feature: Test File Ooperations

  Scenario: Create Files
    Given Create 10 File With Prefix "create"
    Given Wait For 1000 Milliseconds
    Given Remove Files

    Given Create 10 File With Prefix "delete"
    Given Wait For 1000 Milliseconds
    Then Clear Files


  Scenario: Modify Files
    Given Create 10 File With Prefix "modify"
    Given Modify Files
    Given Wait For 1000 Milliseconds

    Then Check Modify Backup File
    Given Remove Files

  Scenario: Deferred Delete
    Given Create 1 File With Prefix "deferred" With Delay 5000 Milliseconds
    Given Wait For 100 Milliseconds
    Then File Must Exist
    Given Wait For 1000 Milliseconds
    Then File Must Not Exist


