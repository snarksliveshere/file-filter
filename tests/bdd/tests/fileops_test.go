//go:build bdd_tests

package bdd_tests

import (
	"fmt"
	"os"
	"time"

	"github.com/cucumber/godog"
)

func fileOpsTest(ctx *godog.ScenarioContext) {
	ctx.Step(`^Create (\d+) File With Prefix "([^"]*)"$`, test.createFileWithPrefix)
	ctx.Step(`^Remove Files$`, test.removeFiles)
	ctx.Step(`^Modify Files$`, test.modifyFiles)
	ctx.Step(`^Check Modify Backup File$`, test.checkModifyBackupFile)
	ctx.Step(`^File Must Exist$`, test.fileMustExist)
	ctx.Step(`^File Must Not Exist$`, test.fileMustNotExist)
	ctx.Step(`^Create (\d+) File With Prefix "([^"]*)" With Delay (\d+) Milliseconds$`,
		test.createFileWithPrefixWithDelayMilliseconds)
	ctx.Step(`^Clear Files$`, test.clearFiles)

}

func (test *bddTest) clearFiles() error {
	test.files = nil
	return nil
}

func (test *bddTest) createFileWithPrefixWithDelayMilliseconds(numFiles int, prefix string, delay int) error {
	t := time.Now().UTC().Add(time.Duration(delay) * time.Millisecond)
	tStr := t.Format(time.RFC3339)
	pref := fmt.Sprintf("%s_%s_%s_", "delete", tStr, prefix)

	err := test.createFileWithPrefix(numFiles, pref)
	if err != nil {
		return err
	}
	return nil
}

func (test *bddTest) fileMustExist() error {
	for _, v := range test.files {
		_, err := os.Stat(v.Name())
		if err != nil {
			return err
		}
	}
	return nil
}

func (test *bddTest) fileMustNotExist() error {
	for _, v := range test.files {
		_, err := os.Stat(v.Name())
		if !os.IsNotExist(err) {
			return err
		}
	}
	return nil
}

func (test *bddTest) checkModifyBackupFile() error {
	for _, v := range test.files {
		name := getBackupNameByHotName(v.Name())

		b, err := os.ReadFile(name)
		if err != nil {
			return err
		}
		if string(b) != test.env.TestHotPath {
			return fmt.Errorf("file:%s content must be identical, instead - expected:%s actual:%s",
				v.Name(), test.env.TestHotPath, string(b))
		}
	}
	return nil
}

func (test *bddTest) modifyFiles() error {
	for _, v := range test.files {
		err := writeToFile(v.Name(), test.env.TestHotPath)
		if err != nil {
			return err
		}
	}
	return nil
}

func (test *bddTest) createFileWithPrefix(numFiles int, prefix string) error {
	for i := 0; i < numFiles; i++ {
		name := fmt.Sprintf("%s_%s", prefix, randSeq(5))
		path := test.getHotFile(name)
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		test.files = append(test.files, file)
	}
	return nil
}

func (test *bddTest) removeFiles() error {
	for _, v := range test.files {
		name := getFileNameFromPath(v.Name())

		name = fmt.Sprintf("delete_%s", name)

		name = test.getHotFile(name)
		err := os.Rename(v.Name(), name)
		if err != nil {
			return err
		}
	}
	test.files = nil
	return nil
}
