//go:build bdd_tests

package bdd_tests

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/cucumber/godog"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func commonTest(ctx *godog.ScenarioContext) {
	ctx.Step(`^Breakpoint$`, test.breakpoint)
	ctx.Step(`^Wait For (\d+) Milliseconds$`, test.waitForMilliseconds)
}

func openFile(name string) (*os.File, error) {
	file, err := os.OpenFile(name,
		os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func writeToFile(name, msg string) error {
	file, err := openFile(name)
	if err != nil {
		return err
	}
	_, err = file.WriteString(msg)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	return nil
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func (test *bddTest) getHotFile(name string) string {
	return fmt.Sprintf("%s/%s", test.env.TestHotPath, name)
}

func (test *bddTest) getBackupFile(name string) string {
	return fmt.Sprintf("%s_%s.bak", test.env.TestBackupPath, name)
}

func getFileNameFromPath(name string) string {
	index := strings.LastIndex(name, "/") + 1
	return name[index:]
}

func getBackupNameByHotName(name string) string {
	index := strings.LastIndex(name, "/") + 1
	fileName := name[index:]
	return fmt.Sprintf("%s/%s.bak", test.env.TestBackupPath, fileName)
}

func (test *bddTest) breakpoint() error {
	return nil
}

func (test *bddTest) waitForMilliseconds(numMilliSeconds int) error {
	time.Sleep(time.Duration(numMilliSeconds) * time.Millisecond)
	return nil
}
