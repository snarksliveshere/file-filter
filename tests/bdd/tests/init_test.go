//go:build bdd_tests

package bdd_tests

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/cucumber/godog"

	"gitlab.com/snarksliveshere/file-filter/tests/bdd/stuff/config"
)

var test *bddTest

type bddTest struct {
	TContext *testing.T
	ctx      context.Context

	files []*os.File

	resp []byte
	env  config.Env
}

func TestMain(m *testing.M) {
	cfg, err := config.GetTestEnv()
	if err != nil {
		panic(err)
	}
	status := godog.TestSuite{
		Name:                 "bdd",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options: &godog.Options{
			Format:        "pretty",
			Paths:         []string{cfg.TestPath},
			Randomize:     0,
			StopOnFailure: false,
		},
	}.Run()

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func InitializeTestSuite(ctx *godog.TestSuiteContext) {
	test = new(bddTest)
	ctx.BeforeSuite(test.start)
	ctx.AfterSuite(test.end)
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	commonTest(ctx)
	fileOpsTest(ctx)
}

func (test *bddTest) start() {
	fmt.Println("**************START....................")
	rand.Seed(time.Now().UnixNano())
	test.TContext = &testing.T{}
	test.ctx, _ = context.WithTimeout(context.Background(), 1*time.Minute)
	cfg := config.GetConfigs()
	test.env = cfg

	fmt.Println("..............START******************")
}
