# File Watcher And Filter

## Run Application
### OS: Linux

Run from repo:   
1. ```go run cmd/filter/main.go```
2. or build file ```go build -o watcher ./cmd/filter/main.go```  
mandatory parameters:   
--hot {path to hot folder}  
--backup {path to backup folder} 

optional parameters:  
--log {path to log file, default: text.log}  
--state {path to state file, default: state.json}  

## How to

Run the application with parameters  

example: ```./watcher --hot {hotPath} --backup {backupPath}```

And then, It is now possible to print in console: some text or regex for fileName.    
Application parses the log and first determines if a regular expression can match to the file name.  
If RegEx doesn't match the file name, the raw input will be searched for.  
For example:  
logFile:
```
[INFO] 2023/03/14 09:33:02  file:[[create_GKeEf] event:[[create] in folder:[[./folders/destination]]
[INFO] 2023/03/14 09:33:02  backup file:[[create_GKeEf.bak]] event:[[create]] in folder:[[./folders/backup]]
[INFO] 2023/03/14 09:33:02  file:[[create_GKIMd] event:[[create] in folder:[[./folders/destination]]
[INFO] 2023/03/14 09:33:02  backup file:[[create_GKIMd.bak]] event:[[create]] in folder:[[./folders/backup]]
```
If you write: `info`  
Output will be:
```
[INFO] 2023/03/14 09:33:02  file:[[create_GKeEf] event:[[create] in folder:[[./folders/destination]]
[INFO] 2023/03/14 09:33:02  backup file:[[create_GKeEf.bak]] event:[[create]] in folder:[[./folders/backup]]
[INFO] 2023/03/14 09:33:03  file:[[create_GKIMd] event:[[create] in folder:[[./folders/destination]]
[INFO] 2023/03/14 09:33:03  backup file:[[create_GKIMd.bak]] event:[[create]] in folder:[[./folders/backup]]
```
`backup`  
```
[INFO] 2023/03/14 09:33:02  backup file:[[create_GKeEf.bak]] event:[[create]] in folder:[[./folders/backup]]
[INFO] 2023/03/14 09:33:03  backup file:[[create_GKIMd.bak]] event:[[create]] in folder:[[./folders/backup]]
```
`09:33:03`
```
[INFO] 2023/03/14 09:33:03  file:[[create_GKIMd] event:[[create] in folder:[[./folders/destination]]
[INFO] 2023/03/14 09:33:03  backup file:[[create_GKIMd.bak]] event:[[create]] in folder:[[./folders/backup]]
```
`Ef$`
```
[INFO] 2023/03/14 09:33:02  file:[[create_GKeEf] event:[[create] in folder:[[./folders/destination]]
```

### Notes

1. Regarding the requirement: `backup files should have the same name of the original file with .bak extension`  
I just added .bak to the end of file name, because there is no such thing as an extension in linux.  
But that's easy to change.
2. `if the file name is prefixed with 'delete_ISODATETIME_' it should be deleted at the
specified time`  Implemented    
3. `You can -although not adviced- use any 3rd party libraries`. Actually, I would prefer to use `https://github.com/fsnotify/fsnotify` library, because it provides cross-platform filesystem notifications on Windows, Linux, macOS.  
But I haven't used it because it's not recommended.
4. With the application running, you can also run bdd tests
```TEST_HOT_PATH={hotPath} TEST_BACKUP_PATH={backupPath} go test ./tests/bdd/... -tags=bdd_tests -count=1 -v```
